terraform {
  backend "s3" {
    bucket = "john0-dev-tfstate"
    key = "john0-dev-tfstate1/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "john-terraform-state-lock"
  }
}


/*
when creating the Dynamp DB table, make sure to create partition key as "LockID".
*/ 
